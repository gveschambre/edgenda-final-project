FROM maven:3.6.0-jdk-8 as builder
COPY src /usr/src/app/src  
COPY pom.xml /usr/src/app
RUN mvn -f /usr/src/app/pom.xml clean package

FROM openjdk:8  
COPY --from=builder /usr/src/app/target/skills-manager-0.0.1-SNAPSHOT.jar  /usr/app/skills-manager-0.0.1-SNAPSHOT.jar    
ENTRYPOINT ["java","-jar","/usr/app/skills-manager-0.0.1-SNAPSHOT.jar"] 