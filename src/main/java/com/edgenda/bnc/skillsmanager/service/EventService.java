package com.edgenda.bnc.skillsmanager.service;

import java.util.Collections;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.edgenda.bnc.skillsmanager.model.Event;
import com.edgenda.bnc.skillsmanager.model.Guest;
import com.edgenda.bnc.skillsmanager.repository.EventRepository;
import com.edgenda.bnc.skillsmanager.repository.GuestRepository;
import com.edgenda.bnc.skillsmanager.service.exception.UnknownEventException;
import com.edgenda.bnc.skillsmanager.service.exception.UnknownGuestException;

@Service
@Transactional
public class EventService {

    private final EventRepository eventRepository;

    private final GuestRepository guestRepository;
    
    private final MailService mailService;

    @Autowired
    public EventService(EventRepository eventRepository, GuestRepository guestRepository, MailService mailService) {
        this.eventRepository = eventRepository;
        this.guestRepository = guestRepository;
        this.mailService = mailService;
    }

    public Event getEvent(Long id) {
        Assert.notNull(id, "Event ID cannot be null");
        return eventRepository.findById(id)
                .orElseThrow(() -> new UnknownEventException(id));
    }

    public List<Event> getEvents() {
        return eventRepository.findAll();
    }

    public Event createEvent(Event event) {
        Assert.notNull(event, "Event cannot be null");
        final Event newEvent = new Event(
                event.getName(),
                event.getStartDate(),
                event.getEndDate(),
                Collections.emptyList()
        );
        return eventRepository.save(newEvent);
    }

    public void updateEvent(Event event) {
        Assert.notNull(event, "Event cannot be null");
        this.getEvent(event.getId());
        eventRepository.save(event);
    }

    public List<Guest> getEventGuests(Long eventId) {
        return guestRepository.findByEventId(eventId);
    }

    public void deleteEvent(Long id) {
        Assert.notNull(id, "ID cannot be null");
        eventRepository.delete(id);
    }

	public Event addGuest(Long eventId, Long guestId) {
		Event event = eventRepository.findById(eventId)
                .orElseThrow(() -> new UnknownEventException(eventId));
		Guest guest = guestRepository.findById(guestId)
                .orElseThrow(() -> new UnknownGuestException(guestId));
		
		if (!guest.getEvents().contains(event)) {
			guest.getEvents().add(event);
			guestRepository.save(guest);
			event = eventRepository.findById(eventId)
	                .orElseThrow(() -> new UnknownEventException(eventId));
			mailService.sendAddGuestMail(guest, event);
		}
		
		return event;
	}

	public Event deleteGuestFromEvent(Long eventId, Long guestId) {
		Event event = eventRepository.findById(eventId)
                .orElseThrow(() -> new UnknownEventException(eventId));
		Guest guest = guestRepository.findById(guestId)
                .orElseThrow(() -> new UnknownGuestException(guestId));
		
		if (guest.getEvents().contains(event)) {
			guest.getEvents().remove(event);
			guestRepository.save(guest);
			event = eventRepository.findById(eventId)
	                .orElseThrow(() -> new UnknownEventException(eventId));
			mailService.sendRemovedGuestMail(guest, event);
		}
		return event;
	}
}
