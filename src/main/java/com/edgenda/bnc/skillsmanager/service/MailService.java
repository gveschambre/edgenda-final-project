package com.edgenda.bnc.skillsmanager.service;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.edgenda.bnc.mailer.Email;
import com.edgenda.bnc.mailer.Mailer;
import com.edgenda.bnc.skillsmanager.model.Event;
import com.edgenda.bnc.skillsmanager.model.Guest;

@Service
public class MailService {

	@Value("${rabbit.host}")
	private String rabbitHostname;

	@Value("${rabbit.port}")
	private int rabbitPort;

	private String senderEmail = "guillaume.veschambre@gmail.com ";
	private String senderName = "Event Manager";

	@Async
	public void sendAddGuestMail(Guest guest, Event event) {
		String subject = "Event scheduled " + event.getName();
		String body = "You have been invited to the event " + event.getName()+" from: " + event.getStartDate()+" to:" + event.getEndDate();
		
		String receiverEmail = guest.getEmail();
		String receiverName = guest.getFirstName() + " " + guest.getLastName();
		Mailer mailer = new Mailer(rabbitHostname, rabbitPort);
		
		Email email = new Email(senderEmail,  senderName,  receiverEmail,
           receiverName,  subject,  body);
		try {
			mailer.send(email);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TimeoutException e) {
			e.printStackTrace();
		}
		
	}

	@Async
	public void sendRemovedGuestMail(Guest guest, Event event) {
		String subject = "Event removed " + event.getName();
		String body = "You have been removed from the event " + event.getName()+" from: " + event.getStartDate()+" to:" + event.getEndDate();
		
		String receiverEmail = guest.getEmail();
		String receiverName = guest.getFirstName() + " " + guest.getLastName();
		Mailer mailer = new Mailer(rabbitHostname, rabbitPort);
		
		Email email = new Email(senderEmail,  senderName,  receiverEmail,
           receiverName,  subject,  body);
		try {
			mailer.send(email);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TimeoutException e) {
			e.printStackTrace();
		}
		
	}

}
