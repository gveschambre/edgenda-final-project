package com.edgenda.bnc.skillsmanager.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.edgenda.bnc.skillsmanager.model.Event;
import com.edgenda.bnc.skillsmanager.model.Guest;
import com.edgenda.bnc.skillsmanager.repository.EventRepository;
import com.edgenda.bnc.skillsmanager.repository.GuestRepository;
import com.edgenda.bnc.skillsmanager.service.exception.UnknownGuestException;

@Service
@Transactional
public class GuestService {

    private final GuestRepository guestRepository;

    private final EventRepository eventRepository;

    @Autowired
    public GuestService(GuestRepository guestRepository, EventRepository eventRepository) {
        this.guestRepository = guestRepository;
        this.eventRepository = eventRepository;
    }

    public Guest getGuest(Long id) {
        Assert.notNull(id, "Guest ID cannot be null");
        return guestRepository.findById(id)
                .orElseThrow(() -> new UnknownGuestException(id));
    }

    public List<Guest> getGuests() {
        return guestRepository.findAll();
    }

    public List<Event> getEventsWithGuest(Long guestId) {
        Assert.notNull(guestId, "Guest ID cannot be null");
        return eventRepository.findByGuestId(guestId);
    }
}

