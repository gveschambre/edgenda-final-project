package com.edgenda.bnc.skillsmanager.rest;

import com.edgenda.bnc.skillsmanager.model.Event;
import com.edgenda.bnc.skillsmanager.model.Guest;
import com.edgenda.bnc.skillsmanager.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/events")
public class EventController {

    private final EventService eventService;

    @Autowired
    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Event getEvent(@PathVariable Long id) {
        return eventService.getEvent(id);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Event> getAllEvents() {
        return eventService.getEvents();
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Event createEvent(@RequestBody Event event) {
        return eventService.createEvent(event);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public void updateEvent(@PathVariable Long id, @RequestBody Event event) {
        eventService.updateEvent(
                new Event(
                        id,
                        event.getName(),
                        event.getStartDate(),
                        event.getEndDate(),
                        event.getGuests()
                )
        );
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public void deleteEvent(@PathVariable Long id) {
        eventService.deleteEvent(id);
    }

    @RequestMapping(path = "/{id}/guests", method = RequestMethod.GET)
    public List<Guest> getEventGuests(@PathVariable Long id) {
        return eventService.getEventGuests(id);
    }
    
    @RequestMapping(path = "/{eventId}/guests/{guestId}", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Event addGuest(@PathVariable Long eventId, @PathVariable Long guestId) {
    	return eventService.addGuest(eventId, guestId);
    }
    
    @RequestMapping(path = "/{eventId}/guests/{guestId}", method = RequestMethod.DELETE)
    public Event deleteGuest(@PathVariable Long eventId, @PathVariable Long guestId) {
    	return eventService.deleteGuestFromEvent(eventId, guestId);
    }

}
