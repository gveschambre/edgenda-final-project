package com.edgenda.bnc.skillsmanager.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.PreRemove;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.PersistenceConstructor;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Event {

    @Id
    @GeneratedValue
    private Long id;

    @NotEmpty
    private String name;

    @NotEmpty
    private String startDate;
    
    @NotEmpty
    private String endDate;

    @ManyToMany(mappedBy = "events")
    @JsonIgnoreProperties({"events", "id", "name"})
    private List<Guest> guests;
    
    public Event() {
    }

    public Event(Long id, String name, String startDate, String endDate, List<Guest> guests) {
        this.id = id;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.guests = guests;
    }

    @PersistenceConstructor
    public Event(String name, String startDate, String endDate, List<Guest> guests) {
    	this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.guests = guests;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getStartDate() {
        return startDate;
    }

    public List<Guest> getGuests() {
        return guests;
    }

    @PreRemove
    private void removeGuestsFromEvent() {
        for (Guest guest : guests) {
            guest.getEvents().remove(this);
        }
    }

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	public Event addGuest(Guest guest) {
		if (!this.guests.contains(guest)) {
			this.guests.add(guest);
		}
		return this;
	}

}
